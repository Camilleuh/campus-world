<?php

namespace App\Repositories;

use App\Models\Parrain;
use Illuminate\Http\Request;

class ParrainRepository {

	/**
	 *
	 * @var Parrain
	 *
	 * */
	private $parrain;

	public function __construct(Parrain $parrain) {
		$this->parrain = $parrain;
	}

	public function  listAllParrains( $limit = 0) {
		// $this->abuse::all();
		$where = [];
		$result = $this->parrain::where($where)
		//->with(['nom','prenom','pays','region','ville','etablissement','filiere','niveau','email','telephone','boursier','compte'])
		->orderBy('created_at', 'asc');
		return ($limit >- 1) ? $result->paginate($limit):$result->paginate($result->count());
	}
	 /**
     * @param int $id
     * @return mixed
     */
	/*public function getAbuse(int $id) {
		return $this->abuse::where(['id' => $id])->with(['comment','offer'])->first();
	}*/
	public function saveParrain(Request $request){
		//$typeAbuse = (int)$request->input('type_abuse',0);
		$data = [
			//'type_abuse' => $typeAbuse,
			'nom' => $request->input('nom'),
			'prenom' => $request->input('prenom'),
			'pays' => $request->input('pays'),
            'region' => $request->input('region'),
            'ville' => $request->input('ville'),
			'etablissement' => $request->input('etablissement'),
			'filiere' => $request->input('filiere'),
            'niveau' => $request->input('niveau'),
            'email' => $request->input('email'),
			'telephone' => $request->input('telephone'),
			'boursier' =>((int)$request->input('boursier',0) == 1)?true:false,
			'compte' =>((int)$request->input('compte',0) == 1)?true:false,

		];
		//if($typeAbuse == 0)$data['offer_id'] =  $request->input('object_id');
		//if($typeAbuse == 1)$data['comment_id'] =  $request->input('object_id');
        $parrain = Parrain::create($data);
        return true;
		//return $this->abuse::where(['id'=>$abuse->id])->with(['comment','offer','user'])->first();//ODO: voir pourquoi avec le with offer, ça ne marche pas.
		//return $this->abuse::where(['id'=>$abuse->id])->first();
	}
	/**
	 * N'est pas utilisée
	 *  */
	/*public function updateAbuse(string $id, Request $request){
		$data = [
			'message' => $request->get('message'),
		];
		$this->abuse::where(['id' => $id])->update($data);
		return $this->abuse::where(['id'=>$id])->with(['comment','offer','user'])->first();
	}*/
	 /**
     * @param int $id
     * @return bool
     */
	/*public function disableAllAbuse(int $id) {
		$result = $this->abuse::where(['id' => $id])->update(['status' => 1]);
		return true;
	}*/

    /**
     * @param int $id
     * @return bool
     */
	/*public function enableAllAbuse(int $id) {
		$result = $this->abuse::where(['id' => $id])->update(['status' => 0]);
		return true;
	}*/
	/*public function deleteParrain($id){
		return $this->parrain::where(['id' => $id])->delete();
	}*/
}