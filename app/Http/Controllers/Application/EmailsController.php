<?php
    use App\Mail\TestEmail;

    $data = ['message' => 'This is a test!'];

    Mail::to('ndougwocamille@gmail.com')->send(new TestEmail($data));
?>