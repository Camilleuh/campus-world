<?php
namespace App\Http\Controllers\Application; // indiquer la location du fichier
use Illuminate\Http\Request; //pour importer 
use Auth;
use App\Http\Controllers\Controller;
use App\Repositories\ParrainRepository;
class ParrainController extends Controller
{
    
    private $parrainRepository;
    
    public function __construct(ParrainRepository $parrainRepository) {
            $this->parrainRepository = $parrainRepository;
            
    }

    protected function saveParrain(Request $request)
    {
       $this->parrainRepository->saveParrain($request);
       $request->session()->flash('success', "Parrain enregistré avec succès");
       return redirect()->back();
    }
    public function listAllParrains(Request $request) {
		$limit = (int)$request->get('limit',10);
		$parrains = $this->parrainRepository->listAllParrains($limit);
               // return response()->json($parrains);
                return view('home.dashboard',['parrains'=>$parrains]);
        }

        public function destroy($id)
        {
            $parrain = Task::findOrFail($id);
        
            $parrain->delete();
        
            Session::flash('flash_message', 'parrain successfully deleted!');
        
            return redirect()->route('home.dashboard');
        }
     /*
        public function deleteParrain($id)
        {

                $findParrain = parrains::find($parrain->id);
                if($findParrain->delete())
                {
                        return redirect()->route ('home.dashboard')
                        ->with ('success','Parrain supprimé avec suuccès!');
        
            }
        return back()->withInput()->with('error','Erreur!');


    }
     */
            
}