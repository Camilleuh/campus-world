<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parrains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom', 100);
            $table->string('prenom', 100)->nullable();
            $table->string('pays', 100);
            $table->string('region', 100);
            $table->string('ville', 100);
            $table->string('etablissement', 100);
            $table->string('filiere', 100);
            $table->string('niveau', 100);
            $table->string('email', 100);
            $table->string('telephone', 100);
            $table->boolean('boursier');
            $table->boolean('compte');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parrains');
    }
}
