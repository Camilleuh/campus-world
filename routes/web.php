<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome');});
    Route::get('/', 'Application\HomeController@index')->name('home');
    Route::get('/mission', 'Application\HomeController@mission')->name('mission_route');
    Route::get('/etudiant', 'Application\HomeController@etudiant')->name('etudiant_route');
    Route::get('/parrain', 'Application\HomeController@parrain')->name('parrain_route');
    Route::get('/offres', 'Application\HomeController@offres')->name('offres_route');
    Route::post('/saveParrain', 'Application\ParrainController@saveParrain')->name('save_parrain_route');
    Route::get('/listAllParrains', 'Application\ParrainController@listAllParrains')->name('get_parrains_route');
    Route::delete('/listAllParrains', 'Application\ParrainController@deleteParrain')->name('delete_parrain_route');
    Route::get('/tests', 'Application\EmailsController@test')->name('test_route');

