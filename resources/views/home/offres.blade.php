@extends('layouts.home')

@section('content')
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/offres.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-8 ftco-animate text-center text-md-left mb-5">
            <h1 class="mb-3 bread">L'enseignement supérieur de qualité pour tous...</h1>
          </div>
        </div>
      </div>
    </div>
    <section class="ftco-section text-center bg-light">
    	<div class="container">
    		<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Nos Offres</h2>
          </div>
        </div>
    		<div class="row d-flex">
	        <div class="col-lg-4 col-md-6 ftco-animate">
	          <div class="block-7">
	            <div class="text-center">
		            <h2 class="mb-3">Forfait Free</h2>
                    <span class="price"><span class="number">15<small>Euros</small> </span></span>		            
		            <div >
                        <p> Recevez le meilleur accompagnement pour vos projets de voyage! Ne ratez aucune actualité en nous suivant sur nos réseaux.</p>

                      </div>
		            <a href="#" class="btn btn-primary d-block px-3 py-3 mb-4">Choisir ce forfait</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-4 col-md-6 ftco-animate">
	          <div class="block-7">
	            <div class="text-center">
		            <h2 class="mb-3">Forfait Premium</h2>
		            <span class="price"><span class="number">150<small>Euros</small> </span></span>
		            <div >
                        <p> Recevez le meilleur accompagnement pour vos projets de voyage! Ne ratez aucune actualité en nous suivant sur nos réseaux.</p>

                      </div>
		            <a href="#" class="btn btn-primary d-block px-3 py-3 mb-4">Choisir ce forfait</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-4 col-md-6 ftco-animate">
	          <div class="block-7">
	            <div class="text-center">
		            <h2 class="mb-3">Forfait Prestige</h2>
					<span class="price"><span class="number">250<small>Euros</small> </span></span>		  
					<div >
                        <p> Recevez le meilleur accompagnement pour vos projets de voyage! Ne ratez aucune actualité en nous suivant sur nos réseaux.</p>

                      </div>
			          <a href="#" class="btn btn-primary d-block px-3 py-3 mb-4">Choisir ce forfait</a>
	            </div>
	          </div>
	        </div>
	      </div>
    	</div>
    </section>

@endsection