@extends('layouts.home')

@section('content')
 <div class="col-md-12 bg-primary col-centered">                         
        @include('front_partials.notification_content')
      </div>
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/EtudiantsInternationaux.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-8 ftco-animate text-center text-md-left mb-5">
            <h1 class="mb-3 bread">Devenir Parrain, une experience exceptionnelle! </h1>
          </div>
        </div>
      </div>
    </div>
     <section class="ftco-section ftco-no-pt ftc-no-pb">
     
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 py-5" >
    				<img src="images/undraw_pair_programming_njlp.svg" class="img-fluid" alt="">
    				
          </div>
          
    			<div class="col-lg-6 py-5">
    						<div class="heading-section ftco-animate mt-5">
                  <div class="container" style="text-align:center">
                    <div class="row block-9">
                      <div class="col-md-12 pr-md-5">
                        <form action="{{ route('save_parrain_route')}}" method="POST">
                          <div class="form-group">
                            <input type="text" name="nom" class="form-control" placeholder="Noms" required>
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="prenom" placeholder="Prénoms" required>
                          </div>
                          <div class="form-group">
                            <select class="form-control countries" name="pays" id="countryId" required>
                            <option value="">Pays</option>
                        </select>
                      </div>
                        <div class="form-group">
                        <select class="form-control states" name="region" id="stateId" required>
                            <option value="">Région</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <select class="form-control cities" name="ville" id="cityId" required>
                            <option value="">Ville</option>
                        </select>
                        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
                        <script src="//geodata.solutions/includes/countrystatecity.js"></script>
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="etablissement" required placeholder="Etablissement/Faculté/Université">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="filiere" required placeholder="Filière">
                          </div>
                          <div class="form-group">
                            <input type="number" class="form-control" name="niveau" required placeholder="Niveau d'études actuel">
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" required placeholder="Email">
                          </div>
                          <div class="form-group">
                            <input type="tel" class="form-control" name="telephone" required placeholder="Contact">
                          </div>
                          <div class="form-group">
                          <label for="bourse">Avez-vous bénéficiez d'une bourse?</label>
                          <select class="form-control" name="boursier" required id="bourse">
                            <option value="1">Oui</option>
                            <option value="0">Non</option>
                          </select>
                          </div>
                          <div class="form-group">
                            <label for="bourse">Avez-vous un compte bancaire dans votre pays de résidence?</label>
                          <select class="form-control"name="compte" id="compte">
                            <option value="1">Oui</option>
                            <option value="0">Non</option>
                          </select>
                          </div>
                          @csrf
                          <div class="form-group">
                            <input type="submit" value="Je confirme ma Candidature" class="btn btn-primary py-3 px-5">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
    		</div>
    	</div>
    </section>

@endsection