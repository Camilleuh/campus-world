@extends('layouts.home')

@section('content')
<section class="ftco-section ftco-no-pt ftc-no-pb">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 py-10" >
        <img src="images/undraw_pair_programming_njlp.svg" class="img-fluid" alt="">
      </div>
      <div class="col-lg-12 py-5">
        <div class="container" style="text-align:center">
          <div class="row block-9">
            <div class="col-md-6 pr-md-5">
              <form action="#">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Noms">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Prénoms">
                </div>
                <div class="form-group">
                  <select name="country" class="countries" id="countryId">
                  <option value="">Pays</option>
              </select>
              <select name="state" class="states" id="stateId">
                  <option value="">Région</option>
              </select>
              <select name="city" class="cities" id="cityId">
                  <option value="">Ville</option>
              </select>
              <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
              <script src="//geodata.solutions/includes/countrystatecity.js"></script>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Etablissement/Faculté/Université">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Filière">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control" placeholder="Niveau d'études actuel">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="telephone" class="form-control" placeholder="Contact">
                </div>
                <div class="form-group">
                <label for="bourse">Avez-vous bénéficiez d'une bourse?</label>
                <select name="bourse" id="bourse">
                  <option value="oui">Oui</option>
                  <option value="non">Non</option>
                </select>
                </div>
                <div class="form-group">
                  <label for="bourse">Avez-vous un compte bancaire dans votre pays de résidence?</label>
                <select name="bourse" id="bourse">
                  <option value="oui">Oui</option>
                  <option value="non">Non</option>
                </select>
                </div>
                <div class="form-group">
                  <input type="submit" value="Je confirme ma Candidature" class="btn btn-primary py-3 px-5">
                </div>
              </form>
            
            </div>
          </div>
        </div>
    </div>
  </div>
</section>

    <section class="ftco-section contact-section ftco-degree-bg">

    </section> 
@endsection