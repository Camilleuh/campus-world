@extends('layouts.home')

@section('content')

      <section class="ftco-section ftco-no-pt ftc-no-pb">
     
        <div class="container">
          <div class="row">
            <div class="col-lg-6 py-5" >
              <img src="images/imd.jpeg" class="img-fluid" alt="">
              
            </div>
            <div class="col-lg-6 py-5">
                  
                    <div class="container" style="text-align:center">
                      <div class="row block-9">
                        <div class="col-md-12 pr-md-5">
                         
                            <h1><span class="subheading"> Notre mission:</span></h1> 
                             <p> Accompagner les étudiants dans les procédures de leurs projets d'études à l'étranger, en mettant à leur disposition de l'information fiable et un suivi particulier.</p>
                             <p> Cet accompagnement se fait par un parrain étudiant dans l'ecole et la filière sollicitéé par le filleul pour assurer la qualité de l'information et faciliter le suivi des différents dossiers d'admission. </p>
                             <P> L'accès aux bourses sera ainsi facilité par les parrains déjà bénéficiaires des dites bourses dans les différentes écoles sollicitées. </p>
                              <!-- <p><a href="{{ route('etudiant_route')}}" class="btn btn-primary px-4 py-3">Je suis étudiant</a></p> -->
                              <p><a href="{{ route('parrain_route')}}" class="btn btn-primary px-4 py-3">Je souhaite parrainer un étudiant</a></p>
                      
                      </div>
                    </div>
                  
            </div>
          </div>
        </div>
      </section>




















    </div>
@endsection