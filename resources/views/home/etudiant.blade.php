@extends('layouts.home')

@section('content')
    <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container" style="text-align:center">
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
            <form action="#">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Noms">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Prénom">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Niveau d'étude">
              </div>
              <div class="form-group">
                <select name="country" class="countries" id="countryId">
                <option value="">Pays</option>
            </select>
            <select name="state" class="states" id="stateId">
                <option value="">Région</option>
            </select>
            <select name="city" class="cities" id="cityId">
                <option value="">Ville</option>
            </select>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
            <script src="//geodata.solutions/includes/countrystatecity.js"></script>
              </div>
              <div class="form-group">
                <input type="telephone" class="form-control" placeholder="Contact">
              </div>
              <div class="form-group">
                <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Votre projet d'études"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" value="Je confirme ma Candidature" class="btn btn-primary py-3 px-5">
              </div>
            </form>
          
          </div>
        </div>
      </div>
    </section> 
@endsection