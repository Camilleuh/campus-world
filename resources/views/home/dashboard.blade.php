@extends('layouts.home')
@section('content')
<table id="showParrain" class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Noms</th>
            <th>Prénoms</th>
            <th>Pays</th>
            <th>Région</th>
            <th>Ville</th>
            <th>Etablissement</th>
            <th>Filière</th>
            <th>Niveau d'études</th>
            <th>Email</th>
            <th>Contact</th>
            <th>Boursier</th>
            <th>Compte bancaire</th>
            <th>Actions</th>
        </tr>
    </thead>
    @foreach($parrains as $row)
    <tr>
        <td>{{$row['id']}}</td>
        <td>{{$row['nom']}}</td>
        <td>{{$row['prenom']}}</td>
        <td>{{$row['pays']}}</td>
        <td>{{$row['region']}}</td>
        <td>{{$row['ville']}}</td>
        <td>{{$row['etablissement']}}</td>
        <td>{{$row['filiere']}}</td>
        <td>{{$row['niveau']}}</td>
        <td>{{$row['email']}}</td>
        <td>{{$row['telephone']}}</td>
        <td>{{$row['boursier']}}</td>
        <td>{{$row['compte']}}</td>
        <td>
            <a href="#" class="btn btn-primary btn-sm">Ecrire</a>
           <a href="#" data-toggle="modal" data-target="#deleteModal" class="btn btn-primary btn-sm">Supprimer</button>
            <div class="col-md-6 text-right">
                
            </div>
        
        </td>
        
    </tr>
    @endforeach
</table>
{{ $parrains->links() }}
@endsection