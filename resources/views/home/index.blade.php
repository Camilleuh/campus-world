@extends('layouts.home')

@section('content')
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-8 ftco-animate text-center text-md-left mb-5">
            <h1 class="mb-3 bread">L'enseignement supérieur de qualité pour tous...</h1>
          </div>
        </div>
      </div>
    </div>
     <section class="ftco-section ftco-no-pt ftc-no-pb">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 py-5" >
    				<img src="images/undraw_pair_programming_njlp.svg" class="img-fluid" alt="">
    				
    			</div>
    			<div class="col-lg-6 py-5">
    						<div class="heading-section ftco-animate mt-5">
	            <h2 class="mb-4">Qui sommes-nous?</h2>
						<p>World campus est une plateforme sociale qui met en relation des étudiants répartis sur tous les continents (parrains) avec les élèves en fin de cycle secondaire ou les étudiants en début de cycle (filleul) désireux de poursuivre leurs études supérieures dans un pays autre que celui d’origine ou de résidence.</p>
	          </div>
    						<div class="nav-link" style="margin-top:-10px">
    					
	            <!-- <p><a href="{{ route('etudiant_route')}}" class="btn btn-primary px-4 py-3">Je suis étudiant</a></p> -->
                <p><a href="{{ route('parrain_route')}}" class="btn btn-primary px-4 py-3">Je souhaite parrainer un étudiant</a></p>
		              	</div>
    				
          </div>
    		</div>
    	</div>
    </section>

@endsection