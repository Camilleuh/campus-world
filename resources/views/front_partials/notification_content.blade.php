<div class="alert__content">
    @if(session('success'))
       <div class="alert alert-success" role="alert">
           <span>
                {{ session('success') }}
           </span>
       </div>
    @endif

    @if(session('error'))
       <div class="alert alert-danger" role="alert">
           <span>
                {{ session('error') }}
           </span>
       </div>
    @endif

    @if(session('warning'))
       <div class="alert alert-warning" role="alert">
           <span>
                {{ session('warning') }}
           </span>
       </div>
    @endif
</div>